module bitbucket.org/snapmartinc/redis-queue

go 1.12

require (
	bitbucket.org/snapmartinc/logger v0.0.0-20190722102907-70e1fed01587
	bitbucket.org/snapmartinc/newrelic-context v1.0.1
	bitbucket.org/snapmartinc/rmq v1.0.2
	bitbucket.org/snapmartinc/trace v0.0.0-20190925102910-0918afc8a51f
	bitbucket.org/snapmartinc/user-service-client v0.0.0-20190722090724-5a6f104cf969
	github.com/denisenkom/go-mssqldb v0.0.0-20190924004331-208c0a498538 // indirect
	github.com/go-redis/redis/v8 v8.0.0-beta.1
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/jinzhu/gorm v1.9.11
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/lib/pq v1.2.0 // indirect
	github.com/newrelic/go-agent v2.14.1+incompatible
	github.com/sirupsen/logrus v1.4.2 // indirect
	golang.org/x/lint v0.0.0-20190313153728-d0100b6bd8b3 // indirect
	google.golang.org/appengine v1.6.3 // indirect
	gopkg.in/redis.v5 v5.2.9 // indirect
	honnef.co/go/tools v0.0.0-20190523083050-ea95bdfd59fc // indirect
)
